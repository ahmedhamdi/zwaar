var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: {
        app_vendors: [
            "react", "react-dom", 'redux', 'react-redux'
        ],
        common: './src/mobile.js'
    },
    // devtool: "eval-source-map", watch: true,
    output: {
        filename: './www/js/dist/[name].min.js',
        chunkFilename: './www/js/dist/[id].[hash].chunk.js',
        publicPath: '/'
    },
    node: {
        fs: 'empty'
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style!css'
            }, {
                test: /\.es6$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015", "stage-0", "react"]
                }
            }, {
                test: /\.jsx/,
                loader: 'babel-loader',
                exclude: /(node_modules|bower_components)/,
                include: path.join(__dirname, 'src')
            }, {
                test: /\.js$/,
                include: path.join(__dirname, 'src'),
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                // query: {    presets: ['es2015'] }
            }, {
                test: /\.html$/,
                loader: "html-loader"
            }
        ]
    },
    resolve: {
        modules: [
            path.resolve('./src'),
            path.resolve('./src/imports'),
            path.resolve('./node_modules')
        ],
        extensions: ['.js', '.es6', '.jsx']
    },
    plugins: [
        new webpack
            .optimize
            .CommonsChunkPlugin({
                // The order of this array matters
                names: [
                    "common", "app_vendors"
                ],
                filenmae: ['[name].js'],
                minChunks: Infinity
            }),
        new webpack.DefinePlugin({ // <-- key to reducing React's size
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack
            .optimize
            .DedupePlugin(), //dedupe similar code
        new webpack
            .optimize
            .UglifyJsPlugin(), //minify everything
        new webpack
            .optimize
            .AggressiveMergingPlugin()
    ]
}