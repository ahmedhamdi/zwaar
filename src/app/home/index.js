import React, { Component } from 'react';
import {Button} from 'antd';
import {NavLink} from 'react-router-dom';
class Home extends Component {
    render() {
        return (
            <div>
                    <h1>welcome in home page </h1>
              <NavLink to="/auth/login">
              <Button type="primary" >Sign in</Button> 
              </NavLink>
                <NavLink to="auth/register">
                 <Button type="primary" >Registeration</Button>
                 </NavLink>
            </div>
        );
    }
}

export default Home;