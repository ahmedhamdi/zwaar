import {required} from './validations'

export const register_form =[
    {
        label:"Email",
        name:"email",
        placeholder:"Email",
        validate: [required],
    },
    {
        label:"Password",
        name:"password",
        placeholder:"Password",
        type:"password",
        validate: [required],
    
    }
]