import React, { Component } from 'react'
import { Table,Button } from 'antd';
import { NavLink } from 'react-router-dom';
const columns = [{
    title: 'Material',
    dataIndex: 'material',
  },
   {
    title: 'Degree',
    dataIndex: 'degree',
  
  }];
  const data = [{
    key: '1',
    material: 'Math',
    degree: 60  },
     {
    key: '2',
    material: 'sience',
    degree: 80,
  },
   {
    key: '3',
    material: 'English',
    degree: 70,
  }];
 
export default class profileComponent extends Component {
  render() {
    return (
      <div>
            <Table columns={columns} dataSource={data} size="middle" />

      </div>
    )
  }
}
