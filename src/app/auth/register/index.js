import React, { Component } from 'react';
import {register_form} from '../../forms'
import { Field, reduxForm } from 'redux-form';
import  Renderfield from '../../components';
import {Form,Row,Col,Button} from 'antd';
import axios from "axios";
class register extends Component {
    registerfunction(values)
    {    
       axios
       .post("https://reqres.in/api/register", values)
        .then(respones => {
                this.props.history.replace("/auth/login");
          })
         
    }
    render() {
        const {handleSubmit} =this.props
        return (
            <div>
                   <Form> 
               {register_form.map((field, key)=> (
                    <Field component={Renderfield} key={key} {...{ ...field}}/>
                ))}
                 <Row >
              <Col xs={{ span: 10, offset: 1 }} lg={{ span: 10, offset: 1 }}>
                <Button type="primary" onClick={handleSubmit(this.registerfunction.bind(this))}>
                 Submit
                </Button>
                </Col>
                </Row>
                </Form> 
                
            </div>
        );
    }
}

export default reduxForm({
    // a unique name for the form
    form: 'LoginForm'
  })(register)