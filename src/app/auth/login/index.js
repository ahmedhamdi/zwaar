import React, { Component } from 'react';
import {login_form} from '../../forms'
import { Field, reduxForm } from 'redux-form';
import  Renderfield from '../../components';
import {Form,Button} from 'antd';
import axios from 'axios';
class Login extends Component {
    submitfunction(values){
        debugger
        console.log(values)
        axios
          .post("https://reqres.in/api/login", values)
          .then(response => {
            debugger;
         
            localStorage.setItem("token", response.data.token);
            // this.props.addUser(response.data);
            this.props.history.replace("/auth/profile");
        
          })
          .catch(function(error) {
            console.log(error);
          });
      }

    render() {
    const   { handleSubmit}=this.props
        return (
            <div>
                  <Form>  
               {login_form.map((field, key)=> (
                    <Field component={Renderfield} key={key} {...{ ...field}}/>
                ))}
                 <Button type="primary" onClick={handleSubmit( this.submitfunction.bind(this))}>
                 Submit
                </Button>
                </Form>  
            </div>
        );
    }
}

export default reduxForm({
    // a unique name for the form
    form: 'LoginForm'
  })(Login)