import React, { Component } from 'react';
import Login from'./login';
import Register from './register';
import profileComponent from './profile_page';
import resultComponent from './result_page';
import { Route } from 'react-router-dom';
export default class AuthComponent extends Component {
    render() {
        const {match}=this.props;
console.log(match.url)
        return (
            <section>
                <Route path={`${match.url}/login`}  component={Login}/>
                <Route path={`${match.url}/register`}  component={Register}/>
                <Route path={`${match.url}/profile`} component={profileComponent}/>
                <Route path={`${match.url}/result`} component={resultComponent}/>
 
             </section>
        );
    }
}

