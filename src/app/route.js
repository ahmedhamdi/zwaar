import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Auth from './auth';
import Home from './home';
class RouteComponent extends Component {
    render() {
        return (
            <Router>
            <Switch>
             <Route exact path="/" component={Home}/>
              <Route  path="/auth" component={Auth} />
              </Switch>
              </Router>
        );
    }
}

export default RouteComponent;