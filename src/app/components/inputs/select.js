import React, { Component } from 'react';
import { Select } from 'antd';
import axios from 'axios';

const Option = Select.Option;
export class select extends Component {
    constructor(props)
    {
        super(props);
        this.state={
            data:[] 
        }
        this.fetch_data(props.url)
    }
    fetch_data (url) {
        axios.get(url)
        .then(({data}) => {
            this.setState({data})
            console.log(data);
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }
    render() {
        const { input: { value, ...input }, label, placeholder} = this.props;
        const {data} = this.state;
        console.log(data)
        return (
            <Select
            showSearch
            value={value == null ? undefined:value}
            style={{ width: 200 }}
            placeholder={placeholder ? placeholder :label}
            optionFilterProp="children"
            {...input}
            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          >
          {data.map((d, key)=>(
             <Option key={key} value={d.id}>{d.name}</Option>)
          )
          }
          </Select>
        );
    }
}