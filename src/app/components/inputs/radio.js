import React, { Component } from 'react';
import { Radio } from 'antd';
const RadioGroup = Radio.Group;

export class radio extends Component {
    render() {
            const {input,type,options}=this.props 
        return (
           <RadioGroup options={options} type={type} {...input}/>
        );
    }
}
