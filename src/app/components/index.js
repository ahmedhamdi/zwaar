import * as Fieldcomponet from "./inputs";
import React, { Component } from 'react';
import {get, isString} from 'lodash';
import {Form} from 'antd';

export default class Renderfield extends Component {
    constructor (props){
        // console.log(props.field, props.field.field)
        super(props);
        this.state={};
        this.Fieldcomponet=get(Fieldcomponet,props.field,Fieldcomponet.text)
    }
    render() {
        const { label, meta: { touched, error } } = this.props;
        let validateStatus = null;
        const stringError = isString(error)
        if (touched && error && stringError) {
            validateStatus = 'error';
        }
        const {Fieldcomponet} = this;
        return (
            <div>
            <Form.Item
            label={label}
            hasFeedback
            validateStatus={validateStatus}
            help={touched && stringError && error} >
            <Fieldcomponet {...this.props} />
        </Form.Item >
            </div>
        );
    }
}

